import React from  'react';
import TodoListItem from '../components/todo-list-item';

const TodoList = () => {

  const items = ['Learn React', 'Build App']
  return (
    <ul>
      <li><TodoListItem /></li>
      <li><TodoListItem /></li>
  </ul>
  );
}; 

export default TodoList;